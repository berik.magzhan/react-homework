import React from 'react';
import DepartmentPost from './DepartmentPost';
import DepartmentsList from './DepartmentsList';
import DepartmentDelete from './DepartmentDelete';

export default class Department extends React.Component{
  constructor(props) {
    super(props);
    this.state = {name: '',didUpdate: false};
  }

  setStateOfParent = (name) => {
    this.setState(name);
  }

  render(){
    return (<div className='container'>
      <DepartmentPost setStateOfParent={this.setStateOfParent} />
      <DepartmentDelete setStateOfParent={this.setStateOfParent} />
      <DepartmentsList didUpdate={this.state.didUpdate} setStateOfParent={this.setStateOfParent} />
    </div>)
  }
}



