import React from "react";
import api from "../../api/Api";

export default class DepartmentsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {departments: [], didUpdate: false};
      }

    componentDidMount(){
        api.get('api/2.0/Departments')
        .then(response => {
            const departments = response.data;
            this.setState({departments});
        })
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.didUpdate !== this.props.didUpdate){
            api.get('api/2.0/Departments')
            .then(response => {
                const departments = response.data;
                this.setState({departments});
                this.props.setStateOfParent({didUpdate: false});
            });
        }
    }

    render(){
        return (
            <div  className="details-container">
                <table className="details-table">
                    <thead><tr><th>Department id</th><th>Department name</th></tr></thead>
                    <tbody>{this.state.departments.map(department => <tr key={department.id}><td>{department.id}</td><td>{department.name}</td></tr>)}</tbody>
                </table>
            </div>
        )
    }
}