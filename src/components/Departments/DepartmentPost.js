import React from 'react';
import api from '../../api/Api';

export default class DepartmentPost extends React.Component{
    constructor(props) {
        super(props);
        this.state = {name: ''};
      }

    handleChange = event => {
        this.setState({name: event.target.value }); 
    }

    handleSubmit = async event => {
        event.preventDefault();
        let name = this.state.name;
        const response = await api.post('api/2.0/Departments',{name});
        this.setState({name: ''});
        this.props.setStateOfParent({didUpdate: true});
    }

    render(){
        return(
            <div className='form-container'>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        <span className='form-label-text'>Department name:</span>
                        <input type="text" name="name" value={this.state.name} onChange={this.handleChange} placeholder="Name of the department" />
                    </label>
                    <button type="submit">Add department</button>
                </form>
            </div>
        )
    }
}