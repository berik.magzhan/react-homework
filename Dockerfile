FROM node:17-alpine as build

#also say
WORKDIR /app

#copy react app to the container
COPY package*.json /app/

#prepare the container for building react
RUN npm install --silent
RUN npm install react-scripts@5.0.0 -g --silent
COPY ./ /app/
RUN npm run build

#run test
RUN CI=true npm test

#prepare nginx
FROM nginx:1.20.2-alpine
COPY --from=build /app/build /usr/share/nginx/html
RUN  rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

#fire up nginx
EXPOSE 80 443
CMD ["nginx","-g","daemon off;"]